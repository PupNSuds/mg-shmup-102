using System;
using NUnit.Framework;
using Microsoft.Xna.Framework;
using shmup102_core;



namespace Tests
{
    public class PlayerTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Move_WhenXAxisNegative_PositionXIncrease()
        {
            Player p = new Player(new Vector2(200,200),"test");
            p.speed = 10;
            InputData id = new InputData();
            id.xAxis=-1;
            p.Move(id, new TimeSpan(0,0,1));
            Assert.AreEqual(210f,p.position.X);
        }

        [Test]
        public void Move_WhenXAxisPositive_PositionXDecrease()
        {
            Player p = new Player(new Vector2(200,200),"test");
            p.speed = 10;
            InputData id = new InputData();
            id.xAxis=1;
            p.Move(id, new TimeSpan(0,0,1));
            Assert.AreEqual(190f,p.position.X);
        }

        [Test]
        public void Move_WhenYAxisNegative_PositionYIncrease()
        {
            Player p = new Player(new Vector2(200,200),"test");
            p.speed = 10;
            InputData id = new InputData();
            id.yAxis=-1;
            p.Move(id, new TimeSpan(0,0,1));
            Assert.AreEqual(210f,p.position.Y);
        }
        [Test]
        public void Move_WhenYAxisPositive_PositionYDecrease()
        {
            Player p = new Player(new Vector2(200,200),"test");
            p.speed = 10;
            InputData id = new InputData();
            id.yAxis=1;
            p.Move(id, new TimeSpan(0,0,1));
            Assert.AreEqual(190f,p.position.Y);
        }

        [Test]
        public void ClampPositionToSafeArea_WhenPositionYIsLessThenSavePlayArea_PositionYIsSavePlayAreaMin()
        {
            Player p = new Player(new Vector2(0,0),"test");
            p.ClampPositionToSafeArea();
            Assert.AreEqual(CoreShmup.safePlayArea.Top,p.position.Y);
        }
        [Test]
        public void ClampPositionToSafeArea_WhenPositionYIsGreaterThenSavePlayArea_PositionYIsSavePlayAreaMax()
        {
            Player p = new Player(new Vector2(0,800),"test");
            p.ClampPositionToSafeArea();
            Assert.AreEqual(CoreShmup.safePlayArea.Bottom,p.position.Y);
        }

        [Test]
        public void ClampPositionToSafeArea_WhenPositionXIsLessThenSavePlayArea_PositionXIsSavePlayAreaMin()
        {
            Player p = new Player(new Vector2(0,0),"test");
            p.ClampPositionToSafeArea();
            Assert.AreEqual(CoreShmup.safePlayArea.Left,p.position.X);
        }
        [Test]
        public void ClampPositionToSafeArea_WhenPositionXIsGreaterThenSavePlayArea_PositionXIsSavePlayAreaMax()
        {
            Player p = new Player(new Vector2(2000,0),"test");
            p.ClampPositionToSafeArea();
            Assert.AreEqual(CoreShmup.safePlayArea.Right,p.position.X);
        }
    }
}