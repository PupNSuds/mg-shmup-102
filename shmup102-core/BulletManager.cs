using BulletMLLib;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Diagnostics;

namespace shmup102_core
{
	//Built referencing https://raw.githubusercontent.com/dmanning23/BulletMLLibQuickStart/master/BulletMLLibQuickStart.SharedProject/MoverManager.cs
    //Responsible for all bullets on screen maybe this guy should be singleton?
	public class BulletManager : IBulletManager
	{
		//Singleton instance
		private static BulletManager instance;
		public List<BaseBullet> bullets = new List<BaseBullet>();

		public List<BaseBullet> rootBullets = new List<BaseBullet>();

		public PositionDelegate playerPositionDelegate;
		private float _timeSpeed = 1.0f;
		private float _scale = 1.0f;

		/// <summary>
		/// How fast time moves in this game.
		/// Can be used to do slowdown, speedup, etc.
		/// </summary>
		/// <value>The time speed.</value>
		public float TimeSpeed { 
			get{return _timeSpeed;}
			set{
				//set my time speed
				_timeSpeed = value;

				//set all the bullet time speeds
				foreach (var item in bullets)
					item.TimeSpeed = _timeSpeed;
			}
		}

		/// <summary>
		/// Change the size of this bulletml script
		/// If you want to reuse a script for a game but the size is wrong, this can be used to resize it
		/// </summary>
		/// <value>The scale.</value>
		public float Scale{ 
			get	{ return _scale; }
			set	{
				//set my scale
				_scale = value;

				//set all the bullet scales
				foreach (var item in bullets)
					item.Scale = _scale;
			}
		}

		private BulletManager()
		{
		}

		public static BulletManager GetInstance(){
			if (instance == null){
				instance = new BulletManager();
			}
			return instance;
		}

		/// <summary>
		/// a mathod to get current position of the player
		/// This is used to target bullets at that position
		/// </summary>
		/// <returns>The position to aim the bullet at</returns>
		/// <param name="targettedBullet">the bullet we are getting a target for</param>
		public Vector2 PlayerPosition(IBullet targettedBullet)
		{
			//just give the player's position
			Debug.Assert(null != playerPositionDelegate);
			return playerPositionDelegate();
		}
		
		public IBullet CreateBullet()
		{
			//create the new bullet
			var bullet = new BaseBullet(this);

			//set the speed and scale of the bullet
			bullet.TimeSpeed = TimeSpeed;
			bullet.Scale = Scale;

			//initialize, store in our list, and return the bullet
			bullet.Init();
			bullets.Add(bullet);
			return bullet;
		}

		/// <summary>
		/// Create a new bullet that will be initialized from a top level node.
		/// These are usually special bullets that dont need to be drawn or kept around after they finish tasks etc.
		/// </summary>
		/// <returns>A shiny new top-level bullet</returns>
		public IBullet CreateTopBullet()
		{
			//create the new bullet
			BaseBullet bullet = new BaseBullet(this);

			//set the speed and scale of the bullet
			bullet.TimeSpeed = TimeSpeed;
			bullet.Scale = Scale;

			//initialize, store in our list, and return the bullet
			bullet.Init();
			rootBullets.Add(bullet);
			return bullet;
		}
		
		public void RemoveBullet(IBullet deadBullet)
		{
			BaseBullet bulletToRemove = deadBullet as BaseBullet;
			if (bulletToRemove != null)
				bulletToRemove.used = false;
		}

		public void Update()
		{
			for (int i = 0; i < bullets.Count; i++){
				bullets[i].Update();
				bullets[i].PostUpdate();
			}

			for (int i = 0; i < rootBullets.Count; i++)
				rootBullets[i].Update();

			RemoveUnusedBullets();
		}

		public void RemoveUnusedBullets()
		{
			for (int i = 0; i < bullets.Count; i++)
			{
				if (!bullets[i].used)
				{
					bullets.Remove(bullets[i]);
					i--;
				}
			}

			//clear out top level bullets
			for (int i = 0; i < rootBullets.Count; i++)
			{
				if (rootBullets[i].TasksFinished())
				{
					rootBullets.RemoveAt(i);
					i--;
				}
			}
		}

		public void Clear()
		{
			bullets.Clear();
			rootBullets.Clear();
		}

		public double Tier()
		{
			return 0.0;
		}
	}
}