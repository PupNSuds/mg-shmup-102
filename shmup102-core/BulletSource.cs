using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using BulletMLLib;

namespace shmup102_core
{
  public class BulletSource
{
    private BaseBullet rootBullet;
    private BulletPattern pattern;
    private IBulletManager bulletManager;

    public BulletSource(BulletPattern pattern, Vector2 position){
      this.bulletManager = BulletManager.GetInstance();  
      this.pattern = pattern;

      Init(position);

    }
    public void Init(Vector2 position){
      if (rootBullet != null)
      {
        bulletManager.RemoveBullet(rootBullet);
        rootBullet = null;
      }

      rootBullet = new BaseBullet(bulletManager);
      rootBullet.X = position.X;
      rootBullet.Y = position.Y;
      rootBullet.InitTopNode(pattern.RootNode);
    }

    public void Update(Vector2 position){
      rootBullet.X = position.X;
      rootBullet.Y = position.Y;
      rootBullet.Update();

    }
    public void Reset()
    {
      if (rootBullet != null)
        foreach (var task in rootBullet.Tasks)
          task.HardReset(rootBullet);
    }

    public bool IsEnded
    {
      get
      {
        if (rootBullet == null)
          return false;
        bool ended = true;
        foreach (var t in rootBullet.Tasks)
          ended &= t.TaskFinished;
        return ended;
      }
    }

  }
}