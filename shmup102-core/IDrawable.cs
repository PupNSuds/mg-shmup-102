using System;
using Microsoft.Xna.Framework;

namespace shmup102_core
{
    public interface IDrawable
    {
        string textureName {get;set;}
        Vector2 position {get;set;}
        int spriteIdx {get;set;}
        float scale {get;set;}
    }
}
