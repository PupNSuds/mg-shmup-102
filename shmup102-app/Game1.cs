﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using shmup102_core;

namespace shmup102_app
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Dictionary<string,TextureData> textures = new Dictionary<string,TextureData>();
        //string[] textureNames = new string[] {"ship1","sideBackground1","middleBackground1"};
        Dictionary<string,BulletMLLib.BulletPattern> bulletmlPatterns = new Dictionary<string, BulletMLLib.BulletPattern>();


        List<shmup102_core.IDrawable> drawables;
        CoreShmup coreShmupEngine;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;  // set this value to the desired width of your window
            graphics.PreferredBackBufferHeight = 720;   // set this value to the desired height of your window
            graphics.ApplyChanges();
            IsFixedTimeStep = true;
            Content.RootDirectory = "Content";
            IsMouseVisible = true;

            coreShmupEngine = new CoreShmup("Content\\BulletMLScripts");
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            textures.Add("ship1",new TextureData("ship1",new Vector2(64,64),this.Content.Load<Texture2D>("ship1")));
            textures.Add("sideBackground1",new TextureData("sideBackground1",new Vector2(370,720),this.Content.Load<Texture2D>("sideBackground1")));
            textures.Add("middleBackground1",new TextureData("middleBackground1",new Vector2(370,720),this.Content.Load<Texture2D>("middleBackground1")));
            textures.Add("purp_diamond_bullet",new TextureData("purp_diamond_bullet",new Vector2(64,64),this.Content.Load<Texture2D>("purp_diamond_bullet")));
            // TODO: use this.Content to load your game content here

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            var inputData = parseInput();
            drawables = coreShmupEngine.Update(gameTime.ElapsedGameTime,inputData);
            
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            foreach (var item in drawables)
            {
              var currentTextureData =  textures[item.textureName];
              //This can probably be calcualted and stored at sprite load time
              var sourceRect = new Rectangle(item.spriteIdx * (int)currentTextureData.spriteSize.X, 0, (int)currentTextureData.spriteSize.X, (int)currentTextureData.spriteSize.Y);
              var scale = 1*item.scale;
              spriteBatch.Draw(currentTextureData.texture,item.position, sourceRect,Color.White,0,currentTextureData.spriteSize/2,scale,SpriteEffects.None,1);              
            }

            spriteBatch.End();


            base.Draw(gameTime);
        }

        private InputData parseInput(){
            InputData result = new InputData();
            if (GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X > 0 
                || Keyboard.GetState().IsKeyDown(Keys.A)
                || Keyboard.GetState().IsKeyDown(Keys.Left)){
                    result.xAxis = 1;
            } else if(GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X < 0 
                || Keyboard.GetState().IsKeyDown(Keys.D)
                || Keyboard.GetState().IsKeyDown(Keys.Right)){
                    result.xAxis = -1;
            }
            if (GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y > 0 
                || Keyboard.GetState().IsKeyDown(Keys.W)
                || Keyboard.GetState().IsKeyDown(Keys.Up)){
                    result.yAxis = 1;
            } else if(GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y < 0 
                || Keyboard.GetState().IsKeyDown(Keys.S)
                || Keyboard.GetState().IsKeyDown(Keys.Down) ){
                    result.yAxis = -1;
            }
            return result;
        }
    }
}
