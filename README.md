# mg-shmup-102
Monogame Shmup 102. 
The Dynasty Warriors of SHMUPs.

## build it

```console
mg-shmup-102\
$ dotnet build
```

## run it

```console
mg-shmup-102\shmup-102-app\
$ dotnet run
```
